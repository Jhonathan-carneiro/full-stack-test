Ansioso para Ver o Projeto em Ação?

Gostaria de ver o projeto em funcionamento, mas não quer lidar com a configuração técnica? 
**Acesse https://moviefront-tm78.vercel.app**

-**Interface de login:**

✅Criar uma interface de login.

✅Adicionar campos para usuário e senha.

✅Implementar a lógica de autenticação.

✅Adicionar feedback para usuário ou senha incorreta.

✅Feedbacks de usuário ou senha incorreta:

✅Implementar feedback visual para indicar quando o usuário ou senha estiverem incorretos.

✅Listagem dos dados de filmes:

✅Exibir uma lista de filmes na interface.

✅Consumir dados de uma API externa para obter os filmes.

✅Mostrar informações básicas de cada filme, como título, imagem, etc.

**Paginação dos dados:**

✅Implementar paginação para a lista de filmes, se necessário.

**Listagem dos dados de Usuários:**

✅Consumir dados da API do backend para obter os usuários.

✅Mostrar informações básicas de cada usuário, como nome, e-mail, etc.

✅Autenticação por Token ou similar:

✅Implementar autenticação por token nos endpoints de consulta de dados.

✅Utilizar tokens JWT ou mecanismo similar para autenticação.

✅Publicação no Vercel.app:

✅Publicar a aplicação front-end no Vercel.app.

Uso de Containers Docker:
❌Utilizar containers Docker para empacotar a aplicação.

**Uso de Testes:**

✅Implementar testes automatizados para o front-end e/ou back-end.

**Build para produção:**

✅Realizar o build da aplicação front-end para produção.

Obrigado pela atenção, estarei a disposição para eventuais duvidas.
